import React, { Component } from "react";
import { initializeApp } from "firebase/app";
//import { getFirestore, collection, getDocs, doc, getDoc } from "firebase/firestore"
import Axios from "axios";
//import { getDatabase, ref, onValue, child, get } from "firebase/database";
import Switch from "react-input-switch";
import { withCookies, Cookies } from "react-cookie";

import volc1 from "./imgs/volc1.png";
import trop1 from "./imgs/trop1.png";
import searchImg from "./imgs/search.png";

class Home extends Component {
  constructor(props) {
    super(props);

    let { cookies } = props;

    let cookiesAll = cookies.getAll();
    let cookieArr = Object.values(cookiesAll);

    console.log("cookieArr", cookieArr);

    /*
    {
      name: "finale of devestation",
      url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/9/8/985453e7-997e-4d77-a338-cc0290791ebe.jpg?1557576905",
    },
    {
      name: "eldritch evolution",
      url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/e/f/efcb00e5-2caa-45c8-ad19-05d45c683d16.jpg?1576384769",
    },
    */

    let cardArr = {
      battle: [],
      low: [],
      mid: [],
      high: [
        {
          title: "1 or less op counterspells",
          list: [
            {
              name: "force of will",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/d/d/dd60b291-0a88-4e8e-bef8-76cdfd6c8183.jpg?1598303900",
            },
            {
              name: "fierce guardianship",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/4/c/4c5ffa83-c88d-4f5d-851e-a642b229d596.jpg?1591319453",
            },
            {
              name: "mana drain",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/b/a/ba874c0c-f66f-4edc-9859-40273487aef0.jpg?1608909310",
            },
          ],
        },
        {
          title: "2 or less powerful tutors",
          list: [
            {
              name: "mystical tutor",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/9/2/9264ecb7-bfc0-495d-aaaa-829045ea858f.jpg?1580014050",
            },
            {
              name: "imperial seal",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/6/0/6023bfe0-84bf-454c-b29d-5d128fe3e6b5.jpg?1562869020",
            },
            {
              name: "vampiric tutor",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/1/8/18bd50f2-c3ba-4217-a2d5-bb771e199706.jpg?1608910005",
            },
            {
              name: "demonic tutor",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/3/b/3bdbc231-5316-4abd-9d8d-d87cff2c9847.jpg?1618695728",
            },
            {
              name: "demonic consultation",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/1/d/1d779f19-3068-4976-b96b-8f93d156900b.jpg?1610146869",
            },
            {
              name: "tainted pact",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/c/5/c513f51b-a0db-4c08-8acc-1e91060b93b7.jpg?1628801908",
            },
            {
              name: "worldly tutor",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/f/b/fb6b6fbe-cb6f-4d0d-b3eb-f9629d6784b3.jpg?1610161852",
            },
            {
              name: "sylvan tutor",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/9/d/9dcad208-cbea-458f-af8d-6f5e9ec32df7.jpg?1562932279",
            },
            {
              name: "neoform",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/9/2/92d8f67e-4f2f-4a1f-b190-7c3f39e477e4.jpg?1557577201",
            },
            {
              name: "gamble",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/c/0/c01c9474-90a1-4c2e-bd90-471ff4fd04e5.jpg?1547517257",
            },
            {
              name: "enlightened tutor",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/0/c/0c9ebec9-3474-4062-9607-2e2a72f78299.jpg?1580013657",
            },
          ],
        },
        {
          title: "4 or less broken ramps",
          list: [
            {
              name: "mana crypt",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/4/d/4d960186-4559-4af0-bd22-63baa15f8939.jpg?1599709515",
            },
            {
              name: "sol ring",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/a/c/acce65cc-9093-45a6-8c86-97edce545050.jpg?1629844546",
            },
            {
              name: "jeweled lotus",
              url: "https://c1.scryfall.com/file/scryfall-cards/normal/front/3/c/3c7de64b-3dc8-47dd-8999-4353b5a3a06f.jpg?1608911508",
            },
          ],
        },
      ],
      cedh: [],
    };

    this.state = {
      darkMode: false,
      bodyHeight: 1000,
      bodyWidth: 1200,
      cardArr,
    };
  }

  componentDidMount() {
    if (this.state.darkMode) {
      document.body.style.backgroundColor = "#212024";
    }
    this.setState({
      bodyWidth: window.innerWidth,
      bodyHeight: window.innerHeight,
    });

    this.tryGetCard();
  }

  tryGetCard = () => {
    console.log("try get card");
    let searchReplace = "demonic tutor";
    Axios.get(
      "https://api.scryfall.com/cards/named?fuzzy=" + searchReplace
    ).then((res) => {
      console.log("searchCard", res);
      //this.setState({ searchCard: res.data });
    });
  };

  render() {
    console.log("this.state", this.state);

    let showtier = this.state.cardArr["high"].map((a, k) => {
      console.log("map", a);
      let imgList = a.list.map((b, l) => {
        console.log("mapb", b);
        return (
          <img src={b.url} height={"279"} width={"200"} className={"cardImg"} />
        );
      });
      return (
        <div style={{ marginLeft: 4 }}>
          <div
            style={{
              fontSize: 16,
              textAlign: "center",
              alignSelf: "stretch",
              marginTop: 3,
              marginBottom: 3,
              //textDecorationLine: "underline",
            }}
          >
            {a.title}
          </div>
          <div style={{ display: "flex", flexDirection: "column" }}>
            {imgList}
          </div>
        </div>
      );
    });

    return (
      <div className={"page"}>
        <div className={"top"}>
          <div className={"topMid"}>
            house EDH
            <div>
              <Switch
                styles={{
                  marginBottom: 10,
                  trackChecked: {
                    backgroundColor: "#555",
                  },
                  buttonChecked: {
                    backgroundColor: "#212024",
                  },
                  height: 16,
                  width: 26,
                }}
                value={this.state.darkMode ? 1 : 0}
                onChange={this.toggleDarkMode}
              />
            </div>
          </div>
        </div>
        <div className={"mid"}>
          <div style={{ marginBottom: 8 }}>test test</div>
        </div>
        <div className={"cardsBox"}>
          <div className={"cardsMenu"}>
            <div className={"cardsMenuPick"}>battlecruiser</div>
            <div className={"cardsMenuPick"}>low</div>
            <div className={"cardsMenuPick"}>mid</div>
            <div className={"cardsMenuPick"}>high</div>
            <div className={"cardsMenuPick"}>*</div>
          </div>
          <div className={"showCards"}>{showtier}</div>
        </div>

        <div className={"footer"}></div>
      </div>
    );
  }
}

export default withCookies(Home);
