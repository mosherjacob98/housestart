/* eslint-env browser */
import React from 'react';
import ReactDOM from 'react-dom';
/*
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
} from 'react-router-dom';
*/
//import { CookiesProvider } from 'react-cookie';

//import Counter from './Counter';
//import Home from './Home';
//import UploadImg from './UploadImg';

//import store from './redux/store';
import './styles.css';

import { CookiesProvider } from 'react-cookie';
import Home from './Home'



const App = () => (
<CookiesProvider>
  <Home/>
</CookiesProvider>
);

/*
<Route path="/uploadImg">
  <UploadImg />
</Route>
*/

ReactDOM.render(<App />, document.getElementById('app'));

module.hot.accept();
