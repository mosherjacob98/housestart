var express = require("express");
var path = require("path");
var compression = require("compression");
var bodyParser = require("body-parser");

var cors = require("cors");
var corsOptions = {
  origin: "*",
  credentials: true,
};

var site = express();
var port = 80;

site.use(compression());
//site.use(cors(corsOptions))
site.use(bodyParser.json()); // support json encoded bodies
site.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//http

var http = require("http");
//var server = http.createServer(options, site);
var server = http.createServer(site);

//ssl
/*
var http = require("http");
var https = require("https");
var fs = require("fs");

var options = {
  key: fs.readFileSync("/etc/letsencrypt/live/www.houseedh.com/privkey.pem"),
  cert: fs.readFileSync("/etc/letsencrypt/live/www.houseedh.com/fullchain.pem"),
  ca: fs.readFileSync("/etc/letsencrypt/live/www.houseedh.com/chain.pem"),
};

var server = https.createServer(options, site);

// this http redirect works

var redirectApp = express(),
  redirectServer = http.createServer(redirectApp);

redirectApp.use(function requireHTTPS(req, res, next) {
  if (!req.secure) {
    console.log("redirect 80 to ssl : " + req.url);
    //console.log("code = " + req.query.code);
    //return res.redirect('https://' + req.headers.host + req.url);
    res.redirect("https://houseedh.com" + req.url);
  }
  next();
});

redirectServer.listen(80);
*/

/*socket io*/

var http = require("http");
var ioApp = express();

ioApp.use(bodyParser.json()); // support json encoded bodies
ioApp.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var ioServer = http.createServer(ioApp);
var io = require("socket.io")(ioServer);
var userNum = 0;
var lobby = [];
var users = [];

ioApp.post("/test", function (req, res) {
  console.log("testing", req.body.text);
  res.send({ num: req.body.num, possibleLocations: [] });
});

ioApp.post("/zabo", function (req, res) {
  console.log("zabo", req.body);
});

// serve site

site.use("/", express.static(path.resolve(__dirname, "dist")));
site.use("*", express.static(path.resolve(__dirname, "dist")));

server.listen(80, function () {
  console.log("http served on port :80");
});
/*
server.listen(443, function () {
  console.log("http served on port :443");
});
*/

/*
ioServer.listen(8080, function () {
  console.log('io served on port :8080');
})
*/
